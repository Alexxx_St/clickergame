﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalCounter : MonoBehaviour
{
    public static int ClickerCount;
    public GameObject CountDisplay;
    public int InternalCookie;


    void Update()
    {
        InternalCookie = ClickerCount;
        CountDisplay.GetComponent<Text>().text = "" + InternalCookie;

    }
}
