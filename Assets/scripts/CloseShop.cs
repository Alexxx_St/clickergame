﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseShop : MonoBehaviour
{
    public GameObject BoosterShop;
    
    public void ClickCloseBtn()
    {
        BoosterShop.SetActive(false);
    }
    public void ClickShopBtn()
    {
        BoosterShop.SetActive(true);
    }
}